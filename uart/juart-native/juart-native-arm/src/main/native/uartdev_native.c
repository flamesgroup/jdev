#include "com_flamesgroup_jdev_uart_jni_UARTDev.h"
#include "helper.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <sys/ioctl.h>

struct uartdev {
    int handler;
    struct termios uart_settings;
};

struct uartdev *uartStruct;

JNIEXPORT jlong JNICALL Java_com_flamesgroup_jdev_uart_jni_UARTDev_open
(JNIEnv *env, jobject obj, jstring jDevice, jint baudRate, jint byteSize, jint parity, jint timeout) {
  char device_path[32];
  int device_path_len = (*env)->GetStringLength(env, jDevice);

  if (device_path_len <= 0) {
    throwDevException(env, "device path is too short");
    return -666;
  } else if (device_path_len >= sizeof (device_path)) {
    throwDevException(env, "device path is too long");
    return -666;
  }
  (*env)->GetStringUTFRegion(env, jDevice, 0, device_path_len, device_path);

  int handler;
  handler = open(device_path, O_RDWR | O_NOCTTY | O_NDELAY);
  if (handler < 0) {
    throwDevException(env, "can't open device");
    return -666;
  }
  uartStruct = (struct uartdev*) malloc(sizeof (struct uartdev));
  uartStruct->handler = handler;
  struct termios settings;
  if (tcgetattr(handler, &settings) != 0) {
    throwDevException(env, "can't get uart struct");
    goto closeDevice;
  } else {
    uartStruct->uart_settings = settings;
    if (ioctl(handler, TIOCEXCL) == -1) {
      throwDevException(env, "can't set TIOCEXCL");
      goto closeDevice;
    }
    int flags = fcntl(handler, F_GETFL, 0);
    flags &= ~O_NDELAY;
    fcntl(handler, F_SETFL, flags);
    if (baudRate != -1) {
      if (cfsetispeed(&settings, baudRate) < 0 || cfsetospeed(&settings, baudRate) < 0) {
        throwDevException(env, "can't set baudRate");
        goto closeDeviceAndRestoreUartStruct;
      }
    }
    if (byteSize != -1) {
      settings.c_cflag &= ~CSIZE;
      settings.c_cflag |= byteSize;
    }
    settings.c_cflag |= (CREAD | CLOCAL);
    settings.c_cflag &= ~CRTSCTS;
    settings.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ECHOCTL | ECHOPRT | ECHOKE | ISIG | IEXTEN);
    settings.c_iflag &= ~(IXON | IXOFF | IXANY | INPCK | IGNPAR | PARMRK | ISTRIP | IGNBRK | BRKINT | INLCR | IGNCR | ICRNL);
#ifdef IUCLC
    settings.c_iflag &= ~IUCLC;
#endif
    settings.c_oflag &= ~OPOST;
    /*
     * Parity bits
     */
    settings.c_cflag &= ~(PARENB | PARODD | CMSPAR); //Clear parity settings
    switch (parity) {
      case 1: {  //Parity ODD
        settings.c_cflag |= (PARENB | PARODD);
        settings.c_iflag |= INPCK;
        break;
      }
      case 2: {  //Parity EVEN
        settings.c_cflag |= PARENB;
        settings.c_iflag |= INPCK;
        break;
      }
      case 3: {  //Parity MARK
        settings.c_cflag |= (PARENB | PARODD | CMSPAR);
        settings.c_iflag |= INPCK;
        break;
      }
      case 4: {  //Parity SPACE
        settings.c_cflag |= (PARENB | CMSPAR);
        settings.c_iflag |= INPCK;
        break;
      }
    }

    settings.c_cc[VMIN] = 0;
    int vTime = timeout / 100;
    if (vTime == 0) {
      settings.c_cc[VTIME] = 1;
    } else {
      settings.c_cc[VTIME] = vTime; /* in units of 0.1 s */
    }
    if (tcsetattr(handler, TCSANOW, &settings) == -1) {//Try to set all settings
      throwDevException(env, "can't set settings");
      goto closeDeviceAndRestoreUartStruct;
    }
  }

  return (jlong) (intptr_t) uartStruct;

closeDeviceAndRestoreUartStruct:
  tcsetattr(uartStruct->handler, TCSANOW, &(uartStruct->uart_settings));

closeDevice:
  close(uartStruct->handler);
  free(uartStruct);
  return -666;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_uart_jni_UARTDev_read__JLjava_nio_ByteBuffer_2II
(JNIEnv *env, jobject obj, jlong uartStruct, jobject data, jint dataOffset, jint dataLength) {
  char *data_ptr = (*env)->GetDirectBufferAddress(env, data);
  if (data_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer");
    return -666;
  }

  int ret = read(((struct uartdev*) (intptr_t) uartStruct)->handler, data_ptr + dataOffset, dataLength);
  if (ret < 0) {
    throwDevException(env, "uart read failed");
  }
  return ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_uart_jni_UARTDev_write__JLjava_nio_ByteBuffer_2II
(JNIEnv *env, jobject obj, jlong uartStruct, jobject data, jint dataOffset, jint dataLength) {
  char *data_ptr = (*env)->GetDirectBufferAddress(env, data);
  if (data_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer");
    return -666;
  }

  int ret = write(((struct uartdev*) (intptr_t) uartStruct)->handler, data_ptr + dataOffset, dataLength);
  if (ret < 0) {
    throwDevException(env, "uart write failed");
  }
  return ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_uart_jni_UARTDev_read__J_3BII
(JNIEnv *env, jobject obj, jlong uartStruct, jbyteArray dataByteArray, jint dataOffset, jint dataLength) {
  int ret = -666;
  if (!dataByteArray) {
    throwIllegalArgumentException(env, "data mustn't be null");
  } else {
    jbyte *dataByte_ptr = (*env)->GetByteArrayElements(env, dataByteArray, NULL);
    if (!dataByte_ptr) {
      throwDevException(env, "can't get pointer to the array elements of data");
    } else {
      ret = read(((struct uartdev*) (intptr_t) uartStruct)->handler, dataByte_ptr + dataOffset, dataLength);
      if (ret < 0) {
        throwDevException(env, "uart read failed");
      }
    }
    if (dataByte_ptr) {
      (*env)->ReleaseByteArrayElements(env, dataByteArray, dataByte_ptr, 0);
    }
  }
  return ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_uart_jni_UARTDev_write__J_3BII
(JNIEnv *env, jobject obj, jlong uartStruct, jbyteArray dataByteArray, jint dataOffset, jint dataLength) {
  int ret = -666;
  if (!dataByteArray) {
    throwIllegalArgumentException(env, "data mustn't be null");
  } else {
    jbyte *dataByte_ptr = (*env)->GetByteArrayElements(env, dataByteArray, NULL);
    if (!dataByte_ptr) {
      throwDevException(env, "can't get pointer to the array elements of data");
    } else {
      ret = write(((struct uartdev*) (intptr_t) uartStruct)->handler, dataByte_ptr + dataOffset, dataLength);
      if (ret < 0) {
        throwDevException(env, "uart write failed");
      }
    }
    if (dataByte_ptr) {
      (*env)->ReleaseByteArrayElements(env, dataByteArray, dataByte_ptr, JNI_ABORT);
    }
  }
  return ret;
}

JNIEXPORT void JNICALL Java_com_flamesgroup_jdev_uart_jni_UARTDev_close
(JNIEnv *env, jobject obj, jlong uartStruct) {
  if (tcsetattr(((struct uartdev*) (intptr_t) uartStruct)->handler, TCSANOW, &(((struct uartdev*) (intptr_t) uartStruct)->uart_settings)) == -1) {
    throwDevException(env, "can't setting old uart struct");
  }
  if (close(((struct uartdev*) (intptr_t) uartStruct)->handler) < 0) {
    throwDevException(env, "can't close");
  }
  free((struct uartdev*) (intptr_t) uartStruct);
}
