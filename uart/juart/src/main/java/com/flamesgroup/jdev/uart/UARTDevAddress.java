package com.flamesgroup.jdev.uart;

import java.util.Objects;

public final class UARTDevAddress {

  private final Type type;
  private final int adapter;

  public UARTDevAddress(final Type type, final int adapter) {
    Objects.requireNonNull(type, "type mustn't be null");
    this.type = type;
    this.adapter = adapter;
  }

  public Type getType() {
    return type;
  }

  public int getAdapter() {
    return adapter;
  }

  public String toDeviceName() {
    String path = "/dev/";
    switch (type) {
      case TTY0:
        path += "tty0";
        break;
      case TTYS:
        path += "ttyS";
        break;
      case TTYUSB:
        path += "ttyUSB";
        break;
      case TTYACM:
        path += "ttyACM";
        break;
      case TTYAMA:
        path += "ttyAMA";
        break;
      case RFCOMM:
        path += "rfcomm";
        break;
    }
    return String.format("%s%d", path, adapter);
  }

  public enum Type {

    TTY0,
    TTYS,
    TTYUSB,
    TTYACM,
    TTYAMA,
    RFCOMM

  }

}
