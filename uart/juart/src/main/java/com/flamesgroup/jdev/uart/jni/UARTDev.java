package com.flamesgroup.jdev.uart.jni;

import com.flamesgroup.jdev.DevClosedException;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.State;
import com.flamesgroup.jdev.uart.IUARTDev;
import com.flamesgroup.jdev.uart.UARTDevAddress;
import com.flamesgroup.jdev.uart.UARTOptions;
import org.scijava.nativelib.NativeLibraryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class UARTDev implements IUARTDev {

  private static final Logger logger = LoggerFactory.getLogger(UARTDev.class);

  private static final boolean isLoadNativeLibrarySuccess;

  private final Lock uartDevLock = new ReentrantLock();

  private State state = State.INIT;
  private long handler;
  private final String deviceName;
  private final UARTOptions options;

  private native long open(final String device, final int baudRate, final int byteSize, final int parity, final int timeout) throws DevException;

  private native int read(final long handler, final ByteBuffer data, final int dataOffset, final int dataLength) throws DevException;

  private native int write(final long handler, final ByteBuffer data, final int dataOffset, final int dataLength) throws DevException;

  private native int read(final long handler, final byte[] data, final int dataOffset, final int dataLength) throws DevException;

  private native int write(final long handler, final byte[] data, final int dataOffset, final int dataLength) throws DevException;

  private native void close(final long handler) throws DevException;

  static {
    String libraryName = "uart-native";

    isLoadNativeLibrarySuccess = NativeLibraryUtil.loadNativeLibrary(UARTDev.class, libraryName);
  }

  public UARTDev(final UARTDevAddress uartDevAddress, final UARTOptions uartOptions) {
    Objects.requireNonNull(uartDevAddress, "uartDevAddress mustn't be null");
    Objects.requireNonNull(uartOptions, "uartOptions mustn't be null");
    if (!isLoadNativeLibrarySuccess) {
      throw new UnsupportedOperationException("native library isn't loaded");
    }
    this.deviceName = uartDevAddress.toDeviceName();
    this.options = uartOptions;
  }

  @Override
  public boolean isOpen() {
    uartDevLock.lock();
    try {
      return state != State.CLOSED;
    } finally {
      uartDevLock.unlock();
    }
  }

  @Override
  public void open() throws DevException {
    logger.info("[{}] - trying to open device [{}]", this, deviceName);
    uartDevLock.lock();
    try {
      switch (state) {
        case INIT:
        case CLOSED:
          final int baudRate = options.getBaudRate() == null ? -1 : options.getBaudRate().getValue();
          final int byteSize = options.getDataBit() == null ? -1 : options.getDataBit().getValue();
          final int parity = options.getParity() == null ? -1 : options.getParity().getValue();
          handler = open(deviceName, baudRate, byteSize, parity, options.getTimeout());
          state = State.OPEN;
          break;
        case OPEN:
          throw new IllegalStateException(String.format("[%s] - device [%s] already open", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      uartDevLock.unlock();
    }
    logger.info("[{}] - open device [{}]", this, deviceName);
  }

  @Override
  public void close() throws DevException {
    logger.info("[{}] - trying to close device [{}]", this, deviceName);
    uartDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          close(handler);
          state = State.CLOSED;
          break;
        case CLOSED:
          logger.info("[{}] - device [{}] already closed", this, deviceName);
          return;
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      uartDevLock.unlock();
    }
    logger.info("[{}] - close device [{}]", this, deviceName);
  }

  @Override
  public void read(final ByteBuffer data) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    uartDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          int res = read(handler, data, data.position(), data.remaining());
          data.position(data.position() + res);
          break;
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      uartDevLock.unlock();
    }
  }

  @Override
  public void write(final ByteBuffer data) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    uartDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          int res = write(handler, data, data.position(), data.remaining());
          data.position(data.position() + res);
          break;
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      uartDevLock.unlock();
    }
  }

  @Override
  public int read(final byte[] data, final int dataOffset, final int dataLength) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    uartDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          return read(handler, data, dataOffset, dataLength);
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      uartDevLock.unlock();
    }
  }

  @Override
  public int write(final byte[] data, final int dataOffset, final int dataLength) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    uartDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          return write(handler, data, dataOffset, dataLength);
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      uartDevLock.unlock();
    }
  }

}
