package com.flamesgroup.jdev.uart;

public final class UARTOptions {

  private final BaudRate baudRate;
  private final DataBit dataBit;
  private final Parity parity;
  private final int timeout;

  public UARTOptions(final BaudRate baudRate, final DataBit dataBit, final Parity parity, final int timeout) {
    this.baudRate = baudRate;
    this.dataBit = dataBit;
    this.parity = parity;
    this.timeout = timeout;
  }

  public BaudRate getBaudRate() {
    return baudRate;
  }

  public DataBit getDataBit() {
    return dataBit;
  }

  public Parity getParity() {
    return parity;
  }

  public int getTimeout() {
    return timeout;
  }

  public enum BaudRate {

    B0(0000000),
    B50(0000001),
    B75(0000002),
    B110(0000003),
    B134(0000004),
    B150(0000005),
    B200(0000006),
    B300(0000007),
    B600(0000010),
    B1200(0000011),
    B1800(0000012),
    B2400(0000013),
    B4800(0000014),
    B9600(0000015),
    B19200(0000016),
    B38400(0000017),
    B57600(0010001),
    B115200(0010002),
    B230400(0010003),
    B460800(0010004),
    B500000(0010005),
    B576000(0010006),
    B921600(0010007),
    B1000000(0010010),
    B1152000(0010011),
    B1500000(0010012),
    B2000000(0010013),
    B2500000(0010014),
    B3000000(0010015),
    B3500000(0010016),
    B4000000(0010017);

    private final int value;

    BaudRate(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

  public enum DataBit {

    CS5(0000000),
    CS6(0000020),
    CS7(0000040),
    CS8(0000060);

    private final int value;

    DataBit(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

  public enum Parity {

    NONE(0),
    ODD(1),
    EVEN(2),
    MARK(3),
    SPACE(4);

    private final int value;

    Parity(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
