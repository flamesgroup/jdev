#include "com_flamesgroup_jdev_i2c_jni_I2CDev.h"
#include "helper.h"
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_i2c_jni_I2CDev_open
(JNIEnv *env, jobject obj, jstring jDevice, jint address) {
  char device_path[32];

  int device_path_len = (*env)->GetStringLength(env, jDevice);
  if (device_path_len <= 0) {
    throwDevException(env, "device path is too short");
    return -666;
  } else if (device_path_len >= sizeof (device_path)) {
    throwDevException(env, "device path is too long");
    return -666;
  }
  (*env)->GetStringUTFRegion(env, jDevice, 0, device_path_len, device_path);

  int handler;
  handler = open(device_path, O_RDWR);
  if (handler < 0) {
    throwDevException(env, "can't open");
    return -666;
  }
  if (ioctl(handler, I2C_SLAVE, address) < 0) {
    throwDevException(env, "can't set device address");
    close(handler);
    return -666;
  }
  return handler;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_i2c_jni_I2CDev_read__ILjava_nio_ByteBuffer_2II
(JNIEnv *env, jobject obj, jint handler, jobject data, jint dataOffset, jint dataLength) {
  char *data_ptr = (*env)->GetDirectBufferAddress(env, data);
  if (data_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer");
    return -666;
  }

  int ret = read(handler, data_ptr + dataOffset, dataLength);
  if (ret < 0) {
    throwDevException(env, "i2c read failed");
  }
  return ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_i2c_jni_I2CDev_write__ILjava_nio_ByteBuffer_2II
(JNIEnv *env, jobject obj, jint handler, jobject data, jint dataOffset, jint dataLength) {
  char *data_ptr = (*env)->GetDirectBufferAddress(env, data);
  if (data_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer");
    return -666;
  }

  int ret = write(handler, data_ptr + dataOffset, dataLength);
  if (ret < 0) {
    throwDevException(env, "i2c write failed");
  }
  return ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_i2c_jni_I2CDev_read__I_3BII
(JNIEnv *env, jobject obj, jint handler, jbyteArray dataByteArray, jint dataOffset, jint dataLength) {
  int ret = -666;
  if (!dataByteArray) {
    throwIllegalArgumentException(env, "data mustn't be null");
  } else {
    jbyte *dataByte_ptr = (*env)->GetByteArrayElements(env, dataByteArray, NULL);
    if (!dataByte_ptr) {
      throwDevException(env, "can't get pointer to the array elements of data");
    } else {
      ret = read(handler, dataByte_ptr + dataOffset, dataLength);
      if (ret < 0) {
        throwDevException(env, "i2c read failed");
      }
    }
    if (dataByte_ptr) {
      (*env)->ReleaseByteArrayElements(env, dataByteArray, dataByte_ptr, 0);
    }
  }
  return ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_i2c_jni_I2CDev_write__I_3BII
(JNIEnv *env, jobject obj, jint handler, jbyteArray dataByteArray, jint dataOffset, jint dataLength) {
  int ret = -666;
  if (!dataByteArray) {
    throwIllegalArgumentException(env, "data mustn't be null");
  } else {
    jbyte *dataByte_ptr = (*env)->GetByteArrayElements(env, dataByteArray, NULL);
    if (!dataByte_ptr) {
      throwDevException(env, "can't get pointer to the array elements of data");
    } else {
      ret = write(handler, dataByte_ptr + dataOffset, dataLength);
      if (ret < 0) {
        throwDevException(env, "i2c write failed");
      }
    }
    if (dataByte_ptr) {
      (*env)->ReleaseByteArrayElements(env, dataByteArray, dataByte_ptr, JNI_ABORT);
    }
  }
  return ret;
}

JNIEXPORT void JNICALL Java_com_flamesgroup_jdev_i2c_jni_I2CDev_close
(JNIEnv *env, jobject obj, jint handler) {
  if (close(handler) < 0) {
    throwDevException(env, "can't close");
  }
}
