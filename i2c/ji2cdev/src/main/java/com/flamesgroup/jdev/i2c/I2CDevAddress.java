package com.flamesgroup.jdev.i2c;

public final class I2CDevAddress {

  private final int adapter;

  public I2CDevAddress(final int adapter) {
    this.adapter = adapter;
  }

  public int getAdapter() {
    return adapter;
  }

  public String toDeviceName() {
    return String.format("/dev/i2c-%d", adapter);
  }

}
