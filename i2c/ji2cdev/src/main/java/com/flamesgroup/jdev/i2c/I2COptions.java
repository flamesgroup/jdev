package com.flamesgroup.jdev.i2c;

public final class I2COptions {

  private final int address;

  public I2COptions(final int address) {
    this.address = address;
  }

  public int getAddress() {
    return address;
  }

}
