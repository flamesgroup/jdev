package com.flamesgroup.jdev.i2c.jni;

import com.flamesgroup.jdev.DevClosedException;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.State;
import com.flamesgroup.jdev.i2c.I2CDevAddress;
import com.flamesgroup.jdev.i2c.I2COptions;
import com.flamesgroup.jdev.i2c.II2CDev;
import org.scijava.nativelib.NativeLibraryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class I2CDev implements II2CDev {

  private static final Logger logger = LoggerFactory.getLogger(I2CDev.class);

  private static final boolean isLoadNativeLibrarySuccess;

  private final Lock i2cDevLock = new ReentrantLock();

  private final String deviceName;
  private final I2COptions options;

  private State state = State.INIT;
  private int handler;

  private native int open(final String device, final int address) throws DevException;

  private native int read(final int handler, final ByteBuffer data, final int dataOffset, final int dataLength) throws DevException;

  private native int write(final int handler, final ByteBuffer data, final int dataOffset, final int dataLength) throws DevException;

  private native int read(final int handler, final byte[] data, final int dataOffset, final int dataLength) throws DevException;

  private native int write(final int handler, final byte[] data, final int dataOffset, final int dataLength) throws DevException;

  private native void close(final int handler) throws DevException;

  static {
    String libraryName = "i2cdev-native";

    isLoadNativeLibrarySuccess = NativeLibraryUtil.loadNativeLibrary(I2CDev.class, libraryName);
  }

  public I2CDev(final I2CDevAddress i2CDevAddress, final I2COptions options) {
    Objects.requireNonNull(i2CDevAddress, "i2CDevAddress mustn't be null");
    if (!isLoadNativeLibrarySuccess) {
      throw new UnsupportedOperationException("native library isn't loaded");
    }
    this.deviceName = i2CDevAddress.toDeviceName();
    this.options = options;
  }

  @Override
  public boolean isOpen() {
    i2cDevLock.lock();
    try {
      return state != State.CLOSED;
    } finally {
      i2cDevLock.unlock();
    }
  }

  @Override
  public void open() throws DevException {
    logger.info("[{}] - trying to open device [{}]", this, deviceName);
    i2cDevLock.lock();
    try {
      switch (state) {
        case INIT:
        case CLOSED:
          handler = open(deviceName, options.getAddress());
          state = State.OPEN;
          break;
        case OPEN:
          throw new IllegalStateException(String.format("[%s] - device [%s] already open", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      i2cDevLock.unlock();
    }
    logger.info("[{}] - open device [{}]", this, deviceName);
  }

  @Override
  public void close() throws DevException {
    logger.info("[{}] - trying to close device [{}]", this, deviceName);
    i2cDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          close(handler);
          state = State.CLOSED;
          break;
        case CLOSED:
          logger.info("[{}] - device [{}] already closed", this, deviceName);
          return;
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      i2cDevLock.unlock();
    }
    logger.info("[{}] - close device [{}]", this, deviceName);
  }

  @Override
  public void read(final ByteBuffer data) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    i2cDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          int res = read(handler, data, data.position(), data.remaining());
          data.position(data.position() + res);
          break;
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      i2cDevLock.unlock();
    }
  }

  @Override
  public void write(final ByteBuffer data) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    i2cDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          int res = write(handler, data, data.position(), data.remaining());
          data.position(data.position() + res);
          break;
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      i2cDevLock.unlock();
    }
  }

  @Override
  public int read(final byte[] data, final int dataOffset, final int dataLength) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    i2cDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          return read(handler, data, dataOffset, dataLength);
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      i2cDevLock.unlock();
    }
  }

  @Override
  public int write(final byte[] data, final int dataOffset, final int dataLength) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    i2cDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          return write(handler, data, dataOffset, dataLength);
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException();
      }
    } finally {
      i2cDevLock.unlock();
    }
  }

}
