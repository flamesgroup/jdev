package com.flamesgroup.jdev.gpio;

public final class SysfsGPIOAddress {

  private final int n;
  private final String name;

  public SysfsGPIOAddress(final int n, final String name) {
    this.n = n;
    this.name = name;
  }

  public SysfsGPIOAddress(final int n) {
    this(n, null);
  }

  public int getN() {
    return n;
  }

  @Override
  public String toString() {
    if (name == null) {
      return Integer.toString(n);
    } else {
      return name + "(" + Integer.toString(n) + ")";
    }
  }

}
