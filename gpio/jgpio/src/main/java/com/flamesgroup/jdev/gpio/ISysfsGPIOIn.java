package com.flamesgroup.jdev.gpio;

import com.flamesgroup.jdev.DevException;

public interface ISysfsGPIOIn {

  boolean getValue() throws DevException;

}
