package com.flamesgroup.jdev.gpio;


import com.flamesgroup.jdev.DevException;

public interface ISysfsGPIO extends ISysfsGPIOIn, ISysfsGPIOOut {

  void export() throws DevException;

  void unexport() throws DevException;

  PinDirection getDirection() throws DevException;

  void setDirection(PinDirection direction) throws DevException;

}
