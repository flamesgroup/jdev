package com.flamesgroup.jdev.gpio;

import com.flamesgroup.jdev.DevException;

public interface ISysfsGPIOOut {

  void setValue(boolean value) throws DevException;

}
