package com.flamesgroup.jdev.gpio;

public enum PinDirection {

  IN("in"),
  OUT("out"),
  /* From GPIO Interfaces Linux Documentation:
     To ensure glitch free operation, values "low" and "high" may be written
     to configure the GPIO as an output with that initial value */
  OUT_LOW("low"),
  OUT_HIGH("high");

  private final String value;

  PinDirection(final String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return value.toUpperCase();
  }

  public static PinDirection getByValue(final String value) {
    for (PinDirection pinDirection : PinDirection.values()) {
      if (pinDirection.getValue().equals(value)) {
        return pinDirection;
      }
    }
    throw new IllegalArgumentException("Unsupported value: " + value);
  }

}
