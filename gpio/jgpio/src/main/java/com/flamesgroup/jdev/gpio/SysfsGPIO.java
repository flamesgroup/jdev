package com.flamesgroup.jdev.gpio;

import com.flamesgroup.jdev.DevException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class SysfsGPIO implements ISysfsGPIO {

  private static final Logger logger = LoggerFactory.getLogger(SysfsGPIO.class);

  private static final Path GPIO_SYSFS_PATH = Paths.get("/sys/class/gpio/");
  private static final Path GPIO_EXPORT_SYSFS_PATH = GPIO_SYSFS_PATH.resolve("export");
  private static final Path GPIO_UNEXPORT_SYSFS_PATH = GPIO_SYSFS_PATH.resolve("unexport");

  private final SysfsGPIOAddress address;
  private final Path gpioPath;
  private final Path gpioDirectionPath;
  private final Path gpioValuePath;

  public SysfsGPIO(final SysfsGPIOAddress sysfsGPIOAddress) {
    Objects.requireNonNull(sysfsGPIOAddress, "sysfsGPIOAddress mustn't be null");
    this.address = sysfsGPIOAddress;
    gpioPath = GPIO_SYSFS_PATH.resolve("gpio" + address.getN());
    gpioDirectionPath = gpioPath.resolve("direction");
    gpioValuePath = gpioPath.resolve("value");
  }

  @Override
  public void export() throws DevException {
    try (BufferedWriter bw = Files.newBufferedWriter(GPIO_EXPORT_SYSFS_PATH)) {
      bw.write(Integer.toString(address.getN()));
    } catch (IOException e) {
      logger.warn("[{}] - export GPIO [{}] fail [{}] - may be already exported", this, address, e.getMessage());
      return;
    }
    logger.debug("[{}] - export GPIO [{}] successful", this, address);
  }

  @Override
  public void unexport() throws DevException {
    try (BufferedWriter bw = Files.newBufferedWriter(GPIO_UNEXPORT_SYSFS_PATH)) {
      bw.write(Integer.toString(address.getN()));
    } catch (IOException e) {
      logger.warn("[{}] - unexport GPIO [{}] fail [{}] - may be already unexported", this, address, e.getMessage());
      return;
    }
    logger.debug("[{}] - unexport GPIO [{}] successful", this, address);
  }

  @Override
  public PinDirection getDirection() throws DevException {
    try (BufferedReader br = Files.newBufferedReader(gpioDirectionPath)) {
      return PinDirection.getByValue(br.readLine());
    } catch (IOException e) {
      throw new DevException("[" + this + "] - can't get direction for GPIO [" + address + "]", e);
    }
  }

  @Override
  public void setDirection(final PinDirection direction) throws DevException {
    try (BufferedWriter bw = Files.newBufferedWriter(gpioDirectionPath)) {
      bw.write(direction.getValue());
    } catch (IOException e) {
      throw new DevException("[" + this + "] - can't set direction for GPIO [" + address + "]", e);
    }
  }

  @Override
  public boolean getValue() throws DevException {
    try (BufferedReader br = Files.newBufferedReader(gpioValuePath)) {
      return br.readLine().equals("1");
    } catch (IOException e) {
      throw new DevException("[" + this + "] - can't get value for GPIO [" + address + "]", e);
    }
  }

  @Override
  public void setValue(final boolean value) throws DevException {
    try (BufferedWriter bw = Files.newBufferedWriter(gpioValuePath)) {
      bw.write(value ? "1" : "0");
    } catch (IOException e) {
      throw new DevException("[" + this + "] - can't set value for GPIO [" + address + "]", e);
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "[" + address + "]";
  }

}
