package com.flamesgroup.jdev.spi;

import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.IDev;

import java.nio.ByteBuffer;

public interface ISPIDev extends IDev {

  void exchange(ByteBuffer data) throws DevException;

}
