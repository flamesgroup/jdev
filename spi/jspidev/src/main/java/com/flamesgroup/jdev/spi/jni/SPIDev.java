package com.flamesgroup.jdev.spi.jni;

import com.flamesgroup.jdev.DevClosedException;
import com.flamesgroup.jdev.DevException;
import com.flamesgroup.jdev.State;
import com.flamesgroup.jdev.spi.ISPIDev;
import com.flamesgroup.jdev.spi.SPIDevAddress;
import com.flamesgroup.jdev.spi.SPIOptions;
import org.scijava.nativelib.NativeLibraryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SPIDev implements ISPIDev {

  private static final Logger logger = LoggerFactory.getLogger(SPIDev.class);

  private static final boolean isLoadNativeLibrarySuccess;

  private final Lock spiDevLock = new ReentrantLock();

  private final String deviceName;
  private final SPIOptions options;

  private State state = State.INIT;
  private int handler;

  private native int open(final String device, final int mode, final int lsb, final int bits, final int speed) throws DevException;

  private native void exchange(final int handler, final ByteBuffer data, int position, int limit) throws DevException;

  private native void close(final int handler) throws DevException;

  static {
    String libraryName = "spidev-native";

    isLoadNativeLibrarySuccess = NativeLibraryUtil.loadNativeLibrary(SPIDev.class, libraryName);
  }

  public SPIDev(final SPIDevAddress spiDevAddress, final SPIOptions options) {
    Objects.requireNonNull(spiDevAddress, "spiDevAddress mustn't be null");
    Objects.requireNonNull(options, "options mustn't be null");
    if (!isLoadNativeLibrarySuccess) {
      throw new UnsupportedOperationException("native library isn't loaded");
    }
    this.deviceName = spiDevAddress.toDeviceName();
    this.options = options;
  }

  @Override
  public boolean isOpen() {
    spiDevLock.lock();
    try {
      return state != State.CLOSED;
    } finally {
      spiDevLock.unlock();
    }
  }

  @Override
  public void open() throws DevException {
    logger.info("[{}] - trying to open device [{}]", this, deviceName);
    spiDevLock.lock();
    try {
      switch (state) {
        case INIT:
        case CLOSED:
          handler = open(deviceName, options.getMode().getValue(), options.getLsb().getValue(), options.getBits(), options.getSpeed());
          state = State.OPEN;
          break;
        case OPEN:
          throw new IllegalStateException(String.format("[%s] - device [%s] already open", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      spiDevLock.unlock();
    }
    logger.info("[{}] - open device [{}]", this, deviceName);
  }

  @Override
  public void close() throws DevException {
    logger.info("[{}] - trying to close device [{}]", this, deviceName);
    spiDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          close(handler);
          state = State.CLOSED;
          break;
        case CLOSED:
          logger.info("[{}] - device [{}] already closed", this, deviceName);
          return;
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      spiDevLock.unlock();
    }
    logger.info("[{}] - close device [{}]", this, deviceName);
  }

  @Override
  public void read(final ByteBuffer data) throws DevException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void write(final ByteBuffer data) throws DevException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int read(final byte[] data, final int dataOffset, final int dataLength) throws DevException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int write(final byte[] data, final int dataOffset, final int dataLength) throws DevException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void exchange(final ByteBuffer data) throws DevException {
    Objects.requireNonNull(data, "data mustn't be null");
    spiDevLock.lock();
    try {
      switch (state) {
        case INIT:
          throw new IllegalStateException(String.format("[%s] - device [%s] isn't open", this, deviceName));
        case OPEN:
          exchange(handler, data, data.position(), data.limit());
          break;
        case CLOSED:
          throw new DevClosedException(String.format("[%s] - device [%s] already closed", this, deviceName));
        default:
          throw new IllegalStateException("mustn't be here");
      }
    } finally {
      spiDevLock.unlock();
    }
  }

}
