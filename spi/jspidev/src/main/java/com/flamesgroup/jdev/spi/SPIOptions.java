package com.flamesgroup.jdev.spi;

public final class SPIOptions {

  private final Mode mode;
  private final Lsb lsb;
  private final int bits;
  private final int speed;

  public SPIOptions(final Mode mode, final Lsb lsb, final int bits, final int speed) {
    this.mode = mode;
    this.lsb = lsb;
    this.bits = bits;
    this.speed = speed;
  }

  public Mode getMode() {
    return mode;
  }

  public Lsb getLsb() {
    return lsb;
  }

  public int getBits() {
    return bits;
  }

  public int getSpeed() {
    return speed;
  }

  public enum Mode {

    SPI_MODE_0(0x00 | 0x00),
    SPI_MODE_1(0x00 | 0x01),
    SPI_MODE_2(0x02 | 0x00),
    SPI_MODE_3(0x02 | 0x01);

    private final int value;

    Mode(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }


  public enum Lsb {

    MSB_FIRST(0),
    LSB_FIRST(1);

    private final int value;

    Lsb(final int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

}
