package com.flamesgroup.jdev.spi;

public final class SPIDevAddress {

  final private int bus;
  final private int chipselect;

  public SPIDevAddress(final int bus, final int chipselect) {
    this.bus = bus;
    this.chipselect = chipselect;
  }

  public int getBus() {
    return bus;
  }

  public int getChipselect() {
    return chipselect;
  }

  public String toDeviceName() {
    return String.format("/dev/spidev%d.%d", bus, chipselect);
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null)
      return false;
    if (obj instanceof SPIDevAddress) {
      SPIDevAddress spiDevAddress = (SPIDevAddress) obj;
      return this.bus == spiDevAddress.bus && this.chipselect == spiDevAddress.chipselect;
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + bus;
    result = prime * result + chipselect;
    return result;
  }

}
