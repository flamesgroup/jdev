#include "com_flamesgroup_jdev_spi_jni_SPIDev.h"
#include "helper.h"
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

JNIEXPORT jint JNICALL Java_com_flamesgroup_jdev_spi_jni_SPIDev_open
(JNIEnv *env, jobject obj, jstring jDevice, jint mode, jint lsb, jint bits, jint speed) {
  char device_path[32];

  int device_path_len = (*env)->GetStringLength(env, jDevice);
  if (device_path_len <= 0) {
    throwDevException(env, "device path is too short");
    return -1;
  } else if (device_path_len >= sizeof (device_path)) {
    throwDevException(env, "device path is too long");
    return -1;
  }
  (*env)->GetStringUTFRegion(env, jDevice, 0, device_path_len, device_path);

  int handler;
  handler = open(device_path, O_RDWR);
  if (handler < 0) {
    throwDevException(env, "can't open");
    return -1;
  }
  if (ioctl(handler, SPI_IOC_WR_MODE, &mode) < 0) {
    throwDevException(env, "can't set SPI_IOC_WR_MODE");
    close(handler);
    return -1;
  }
  if (ioctl(handler, SPI_IOC_WR_LSB_FIRST, &lsb) < 0) {
    throwDevException(env, "can't set SPI_IOC_WR_LSB_FIRST");
    close(handler);
    return -1;
  }
  if (ioctl(handler, SPI_IOC_WR_BITS_PER_WORD, &bits) < 0) {
    throwDevException(env, "can't set SPI_IOC_WR_BITS_PER_WORD");
    close(handler);
    return -1;
  }
  if (ioctl(handler, SPI_IOC_WR_MAX_SPEED_HZ, &speed) < 0) {
    throwDevException(env, "can't set SPI_IOC_WR_MAX_SPEED_HZ");
    close(handler);
    return -1;
  }
  return handler;
}

JNIEXPORT void JNICALL Java_com_flamesgroup_jdev_spi_jni_SPIDev_exchange
(JNIEnv *env, jobject obj, jint handler, jobject data, jint position, jint limit) {
  struct spi_ioc_transfer xfer;
  char *data_ptr = (*env)->GetDirectBufferAddress(env, data);
  if (data_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer");
    return;
  }

  memset(&xfer, 0, sizeof (xfer));

  xfer.tx_buf = (unsigned long) data_ptr + position;
  xfer.rx_buf = (unsigned long) data_ptr + position;
  xfer.len = limit - position;

  if (ioctl(handler, SPI_IOC_MESSAGE(1), &xfer) < 0) {
    throwDevException(env, "can't set SPI_IOC_MESSAGE(1)");
  }
  return;
}

JNIEXPORT void JNICALL Java_com_flamesgroup_jdev_spi_jni_SPIDev_close
(JNIEnv *env, jobject obj, jint handler) {
  if (close(handler) < 0) {
    throwDevException(env, "can't close");
  }
  return;
}
