# jdev

Java library to interact with different linux devices.

## Overview

**jdev** implements JNI wrappers over linux I2C, SPI and UART devices subsystems and abstraction over GPIO sysfs interface.

## Structure

**jdev** uses multiple modules [Maven](https://maven.apache.org) project layout.
Where top modules are:
* `commons` - common Java interfaces, exceptions and enums with native JNI helper functions
* `gpio` - java implementation of GPIO is based on [GPIO sysfs interface](https://www.kernel.org/doc/Documentation/gpio/sysfs.txt)
* `i2c` - java and native implementations of I2C based on [linux I2C `/dev` interface](https://www.kernel.org/doc/Documentation/i2c/dev-interface)
* `spi` - java and native implementations of SPI based on [linux SPI `/dev` interface](https://www.kernel.org/doc/Documentation/spi/spidev)
* `uart` - java and native implementations of UART based on linux serial/terminal interface

Almost each top modules consist of main java and auxiliary native submodules. This is made to simplify build process.

## Build

### Build native and java code

To build native libraries for current platform (for now only ARM supported) and install java libraries with native libraries as resource in project main directory execute next command:
```
$ mvn install -P native
```

### Build java code

To build and install java libraries with native libraries as resource in project main directory execute next command:
```
$ mvn install
```

## Usage examples

### GPIO

```java
ISysfsGPIO io = new SysfsGPIO(new SysfsGPIOAddress(100))
io.export();
io.setDirection(PinDirection.OUT);
io.setValue(true);
io.setDirection(PinDirection.IN);
boolean value = io.getValue();
io.unexport();
```

### I2C

```java
ByteBuffer byteBufferRead = ByteBuffer.allocate(100);
ByteBuffer byteBufferWrite = ByteBuffer.allocate(100);
byte[] byteArrayRead = new byte[50];
byte[] byteArrayWrite = new byte[50];

II2CDev i2CDev = new I2CDev(new I2CDevAddress(1), new I2COptions(0x32));
i2CDev.open();
i2CDev.read(byteBufferRead);
i2CDev.write(byteBufferWrite);
i2CDev.read(byteArrayRead, 0, byteArrayRead.length);
i2CDev.write(byteArrayWrite, 10 , 15);
i2CDev.close();
```

### SPI

```java
ByteBuffer byteBufferExchange = ByteBuffer.allocate(100);
SPIDev spiDev1 = new SPIDev(spiDevAddress, new SPIOptions(SPIOptions.Mode.SPI_MODE_1, SPIOptions.Lsb.MSB_FIRST, 0, SPI_DEV_CLK));
spiDev1.open();
spiDev1.exchange(byteBufferExchange);
spiDev1.close();
```

### UART

```java
ByteBuffer byteBufferRead = ByteBuffer.allocate(100);
ByteBuffer byteBufferWrite = ByteBuffer.allocate(100);
byte[] byteArrayRead = new byte[50];
byte[] byteArrayWrite = new byte[50];

IUARTDev uartDev = new UARTDev(new UARTDevAddress(UARTDevAddress.Type.TTYACM, 0), new UARTOptions(UARTOptions.BaudRate.B115200, UARTOptions.DataBit.CS8, UARTOptions.Parity.NONE, 10));
uartDev.open();
uartDev.read(byteBufferRead);
uartDev.write(byteBufferWrite);
uartDev.read(byteArrayRead, 0, byteArrayRead.length);
uartDev.write(byteArrayWrite, 10 , 15);
uartDev.close();
```

## Questions

If you have any questions about **jdev**, feel free to create issue with it.
