#include "jni.h"

void throwException(JNIEnv *pEnv, const char *clazzName, const char *message);

void throwDevException(JNIEnv *pEnv, const char *whereThrow);

void throwIllegalArgumentException(JNIEnv *pEnv, const char *message);
