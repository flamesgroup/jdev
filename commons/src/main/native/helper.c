#include "helper.h"
#include <errno.h>

void throwException(JNIEnv *pEnv, const char *clazzName, const char *message) {
  jclass clazz = (*pEnv)->FindClass(pEnv, clazzName);
  if (clazz) {
    (*pEnv)->ThrowNew(pEnv, clazz, message);
  }
  (*pEnv)->DeleteLocalRef(pEnv, clazz);
}

void throwDevException(JNIEnv *pEnv, const char *whereThrow) {
  const char *clazzName = "com/flamesgroup/jdev/DevException";
  char message[255];
  sprintf(message, "[%s] - %s, errno [%d]", whereThrow, strerror(errno), errno);
  throwException(pEnv, clazzName, message);
}

void throwIllegalArgumentException(JNIEnv *pEnv, const char *message) {
  const char *clazzName = "java/lang/IllegalArgumentException";
  throwException(pEnv, clazzName, message);
}
