package com.flamesgroup.jdev;

import java.nio.ByteBuffer;

public interface IDev {

  boolean isOpen();

  void open() throws DevException;

  void close() throws DevException;

  void read(ByteBuffer data) throws DevException;

  void write(ByteBuffer data) throws DevException;

  int read(byte[] data, int dataOffset, int dataLength) throws DevException;

  int write(byte[] data, int dataOffset, int dataLength) throws DevException;

}
