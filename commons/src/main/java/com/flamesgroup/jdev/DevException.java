package com.flamesgroup.jdev;

public class DevException extends Exception {

  private static final long serialVersionUID = -8704525302100696777L;

  public DevException() {
    super();
  }

  public DevException(final String message) {
    super(message);
  }

  public DevException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public DevException(final Throwable cause) {
    super(cause);
  }

}
