package com.flamesgroup.jdev;

public class DevClosedException extends DevException {

  private static final long serialVersionUID = -2718240079625788391L;

  public DevClosedException() {
    super();
  }

  public DevClosedException(final String message) {
    super(message);
  }

  public DevClosedException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public DevClosedException(final Throwable cause) {
    super(cause);
  }

}
